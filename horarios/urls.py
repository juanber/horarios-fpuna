"""horarios URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import patterns, include
from django.contrib.admin import AdminSite
from django.utils.translation import ugettext_lazy
from app import views
import settings


handler404 = 'app.views.handler404'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
   #url(r'^admin/', admin_site.urls),
   url(r'^$', views.organizar),
   url(r'^materias/', views.get_lista),
]

#if settings.DEBUG:
    # static files (images, css, javascript, etc.)
urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))

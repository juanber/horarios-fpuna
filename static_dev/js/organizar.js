$(document).ready(function(){
  actualizar_variables();
});
function _procesar(elem){
    $((elem.id=='tab2')?'#lista_materias':'#mis_materias').hide("slow" );
    $((elem.id=='tab1')?'#lista_materias':'#mis_materias').show("slow" );
    $((elem.id=='tab2')?'#tab1':'#tab2').removeClass("active");
    $((elem.id=='tab1')?'#tab1':'#tab2').addClass("active");
}

function borrar_seleccionados(){
  $('#tabla_clases > tbody  > tr').find("input:checked").each(function() {
        this.parentNode.parentNode.remove();
  });

  $('#tabla_examenes > tbody  > tr').find("input:checked").each(function() {
        this.parentNode.parentNode.remove();
  });

  actualizar_variables();
}
function actualizar_variables(){

  var aux='';
  var cont=1;
  cont=1;
  $('#tabla_clases > tbody  > tr').each(function() {
        cont2=0;
        $(this).find('td').each(function(){
          if (this.innerHTML.indexOf('checkbox') == -1)
              aux+= '<input type="hidden" name="clase-'+cont.toString()+'" value="'+this.innerHTML+'" /> ';
        }) ;
        cont++;
  });


  aux+= '<input type="hidden" name="cant_clase" value="'+(cont-1).toString()+'" /> ';
  cont=1;
  $('#tabla_examenes > tbody  > tr').each(function() {
        cont2=0;
        $(this).find('td').each(function(){
          if (this.innerHTML.indexOf('checkbox') == -1)
              aux+= '<input type="hidden" name="examen-'+cont.toString()+'" value="'+this.innerHTML+'" /> ';
        }) ;
        cont++;
  });
  aux+= '<input type="hidden" name="cant_examen" value="'+(cont-1).toString()+'" /> ';
  $('#variables').html(aux);
}

function agregar_materias(){
    aux = []
    $('#tabla_materias > tbody  > tr').find("input:checked").each(function() {
      table=document.getElementById('tabla_clases').getElementsByTagName('tbody')[0];;
      index = this.name.split("-")[1]
      aux.push($('#td-materia-'+index).html());

      //procesar horarios de clases
      row=table.insertRow(table.rows.length);
      row_content = '<td class="dontprint" ><input type="checkbox" /></td>';
      row_content+='<td>'+$('#td-materia-'+index).html()+'</td>';
      row_content+='<td>'+$('#td-lunes-'+index).html()+'</td>';
      row_content+='<td>'+$('#td-martes-'+index).html()+'</td>';
      row_content+='<td>'+$('#td-miercoles-'+index).html()+'</td>';
      row_content+='<td>'+$('#td-jueves-'+index).html()+'</td>';
      row_content+='<td>'+$('#td-viernes-'+index).html()+'</td>';
      row_content+='<td>'+$('#td-sabado-'+index).html()+'</td>';
      row.innerHTML=row_content;



      //procesar examenes
      table2=document.getElementById('tabla_examenes').getElementsByTagName('tbody')[0];;
      row2=table2.insertRow(table2.rows.length);
      row_content2 = '<td class="dontprint" ><input type="checkbox" /></td>';
      row_content2+='<td>'+$('#td-materia-'+index).html()+'</td>';
      row_content2+='<td>'+$('#td-pparcial-'+index).html()+'</td>';
      row_content2+='<td>'+$('#td-sparcial-'+index).html()+'</td>';
      row_content2+='<td>'+$('#td-pfinal-'+index).html()+'</td>';
      row_content2+='<td>'+$('#td-sfinal-'+index).html()+'</td>';
      row2.innerHTML=row_content2;

      this.parentNode.parentNode.remove();
    });
    if(aux.length>0){
      $('#mensajes_list').show('slow');
      $('#mensaje').html('"'+aux.join(', ')+'" agregado con exito.');
    }


    actualizar_variables();
}

  $('#carrera').change(
      function(){
           $(this).closest('form').trigger('submit');
      });


  

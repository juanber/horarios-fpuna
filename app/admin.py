from django.contrib import admin
from app.models import Materia,Horario,Carrera
from horarios.settings import MEDIA_ROOT
import xlrd
from django.db import connection
from datetime import datetime

def parseInt(n):
    try:
        n = int(float(n))
    except:
        n = None
    return n
_HORAS = {
	'0.625':'15:00',
	'0.75':'18:00',
	'0.8125':'19:30'

}

def cargar_datos(excel_file):
    workbook = xlrd.open_workbook(excel_file)
    all_worksheets = workbook.sheet_names()
    cursor = connection.cursor()
    cursor.execute("DELETE FROM app_materia")
    cursor.execute("DELETE FROM app_carrera")
    carreras = []

    for worksheet_name in all_worksheets:

        worksheet = workbook.sheet_by_name(worksheet_name)
        nombre = ''.join([worksheet_name,'.csv'])
        print nombre
        if len(nombre)<10:
            carreras.append(nombre[0:-4])
            for rownum in xrange(worksheet.nrows):
                if rownum > 10 :
                    lista = []
                    for entry  in worksheet.row_values(rownum):
                        valor=unicode(entry).encode("utf-8")
                        valor=(valor).replace("\n","-")
                        lista.append(valor)
                    materia = Materia()
                    materia.item = parseInt(lista[0])
                    materia.departamento = lista[1]
                    materia.asignatura = lista[2]
                    materia.nivel = parseInt(lista[3])
                    materia.semestre = parseInt(lista[4])
                    materia.carrera = lista[5]
                    materia.enfasis = lista[6]
                    materia.plan = parseInt(lista[7])
                    materia.turno = lista[8]
                    materia.seccion = lista[9]
                    materia.titulo = lista[10]
                    materia.apellido = lista[11]
                    materia.nombre = lista[12]
                    materia.pparcial_fecha = lista[13]
                    materia.pparcial_hora = _HORAS.get(str(lista[14]),lista[14])
                    materia.sparcial_fecha = lista[15]
                    materia.sparcial_hora = _HORAS.get(str(lista[16]),lista[16])
                    materia.pfinal_fecha = lista[17]
                    materia.pfinal_hora = _HORAS.get(str(lista[18]),lista[18])
                    materia.sfinal_fecha = lista[19]
                    materia.sfinal_hora = _HORAS.get(str(lista[20]),lista[20])
                    materia.lunes = lista[21]
                    materia.martes = lista[22]
                    materia.miercoles = lista[23]
                    materia.jueves = lista[24]
                    materia.viernes = lista[25]
                    materia.sabado = lista[26]
                    materia.fecha_creacion = datetime.now()
                    materia.save()
    for c in carreras:
        carrera = Carrera()
        carrera.codigo=c
        carrera.save()


class MateriaAdmin(admin.ModelAdmin):
    list_display = ('asignatura', 'seccion','titulo','nombre','apellido', 'carrera')
    list_filter = ('carrera',)
    search_fields = ('asignatura','nombre','apellido')
admin.site.register(Materia, MateriaAdmin)



class CarreraAdmin(admin.ModelAdmin):
    list_display = ('codigo','nombre')
admin.site.register(Carrera, CarreraAdmin)



class HorarioAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'archivo','fecha_creacion')
    fields = ('fecha','archivo',)

    def save_model(self, request, obj, form, change):
        obj.save()
        print obj.archivo
        cargar_datos(MEDIA_ROOT+str(obj.archivo))

admin.site.register(Horario, HorarioAdmin)

from django.db import models
from django.utils import timezone

class Carrera(models.Model):
    codigo = models.CharField(max_length=4)
    nombre = models.CharField(max_length=40,null=True)

class Horario(models.Model):
    fecha = models.DateTimeField(default=timezone.now)
    archivo = models.FileField(upload_to='%Y/')
    fecha_creacion = models.DateTimeField(default=timezone.now)
    list_display_links = None

class Materia(models.Model):
    item = models.IntegerField()
    departamento = models.CharField(max_length=5,null=True)
    asignatura = models.CharField(max_length=100,null=True)
    nivel = models.IntegerField(null=True)
    semestre = models.IntegerField(null=True)
    carrera = models.CharField(max_length=5,null=True)
    enfasis = models.CharField(max_length=30,null=True)
    plan = models.IntegerField(null=True)
    turno =  models.CharField(max_length=1,null=True)
    seccion =  models.CharField(max_length=3,null=True)
    titulo = models.CharField(max_length=8,null=True)
    apellido = models.CharField(max_length=50,null=True)
    nombre = models.CharField(max_length=50,null=True)
    pparcial_fecha = models.CharField(max_length=50,null=True)
    pparcial_hora = models.CharField(max_length=10,null=True)
    sparcial_fecha = models.CharField(max_length=50,null=True)
    sparcial_hora =models.CharField(max_length=10,null=True)
    pfinal_fecha = models.CharField(max_length=50,null=True)
    pfinal_hora = models.CharField(max_length=10,null=True)
    sfinal_fecha = models.CharField(max_length=50,null=True)
    sfinal_hora = models.CharField(max_length=10,null=True)
    lunes = models.CharField(max_length=15,null=True)
    martes = models.CharField(max_length=15,null=True)
    miercoles = models.CharField(max_length=15,null=True)
    jueves = models.CharField(max_length=15,null=True)
    viernes = models.CharField(max_length=15,null=True)
    sabado = models.CharField(max_length=15,null=True)
    activo = models.BooleanField(default=True)

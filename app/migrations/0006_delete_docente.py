# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-25 20:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20160325_2038'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Docente',
        ),
    ]

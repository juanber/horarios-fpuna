# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import JsonResponse
from app.models import Materia,Horario,Carrera

from django.shortcuts import render_to_response
from django.template import RequestContext


def handler404(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response

def get_lista(request):
    current_carrera = request.POST.get('carrera','')
    lista = []
    if current_carrera != '':
        materias = Materia.objects.filter(carrera__icontains=current_carrera)
        for m in materias:
            aux = m.__dict__
            del aux['_state']
            lista.append(aux)
    return JsonResponse({'lista':lista})

def organizar(request):
    horario = Horario.objects.last()
    print horario.archivo
    carreras = [c.codigo for c in Carrera.objects.all()]
    context = {
            'ultima_actualizacion':horario.fecha_creacion if horario else 'No hay datos',
            'carreras':" ".join(carreras),
            'descarga':('/media/'+str(horario.archivo) if horario else '')
            }
    return render(request, 'organizar.html', context)

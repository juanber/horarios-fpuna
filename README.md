# Organizador de Horarios

Este sistema permite una mejor visualizacion del horario de clases para 
una mejor organizacion.


Este proyecto depende exclusivamente del horario de clases que se encuentra
en el link http://www.pol.una.py/?q=horario_clases

Este sistema funcionará siempre y cuando el departamento académico mantenga
el formato del horario de clases

## Run
1. instalar dependecias.
	sudo pip install django==1.9
	sudo pip install xlrd
	
2. Ir al directorio del proyecto:
	cd horarios-fpuna
	python manage.py runserver
	
para correr en apache seguir este tutorial:
https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-apache-and-mod_wsgi-on-ubuntu-14-04
## Usage

1. Login.
user: admin
password: admin123

2. Ir a http://localhost:8000/upload/ y cargar el nuevo horario con extensión .xls .

3. Ir a http://localhost:8000/organizar/  y voilà.



## Credits
Juan Duarte
juanber@xionict.com

